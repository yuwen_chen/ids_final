# ids_final

## Step 1
The individual project 1 is extended from the week 1 mini project (i.e. Zola website)

## Step 2
The existing the website is then deployed via Netlify

## Step 3
Update the base url in the "config.toml" is: https://ids721-individual-project-yuwenchen.netlify.app/

## Step 4
Result

![image](image_1.png)
![image](image_2.png)
![image](image_3.png)