+++
title = "Blog for IDS721 Mini Project"
description = "IDS721 Mini Project list"
date = 2021-04-20T09:19:42+00:00
updated = 2021-04-20T09:19:42+00:00
draft = false
template = "blog/page.html"

[taxonomies]
authors = ["Yuwen Chen"]

[extra]
lead = "Belows are the links to existing mini project Github"
+++

## Headings
#### Project 2: https://gitlab.com/yuwen_chen/week2project/-/tree/main
#### Project 3: https://gitlab.com/yuwen_chen/ids721_week3/-/tree/main
#### Project 4: https://gitlab.com/yuwen_chen/ids721_week4/-/tree/main
#### Project 5: https://gitlab.com/yuwen_chen/ids721_week5/-/tree/main
#### Project 6: https://gitlab.com/yuwen_chen/ids721_week6/-/tree/main
