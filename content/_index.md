+++
title = "Official page for IDS721"


# The homepage contents
[extra]
lead = '<b>Author:</b> Yuwen Chen'
url = "/docs/getting-started/introduction/"
url_button = "Get started"
repo_version = "GitHub v0.1.0"
repo_license = "Open-source MIT License."
repo_url = "https://github.com/aaranxu/adidoks"

# Menu items
[[extra.menu.main]]
name = "Docs"
section = "docs"
url = "/docs/getting-started/introduction/"
weight = 10

[[extra.menu.main]]
name = "Blog"
section = "blog"
url = "/blog/"
weight = 20

[[extra.list]]
title = "Week 1"
content = "Placeholder for week 1 assignment"

[[extra.list]]
title = "Week 2"
content = "Placeholder for week 2 assignment"

[[extra.list]]
title = "Week 3"
content = "Placeholder for week 3 assignment"

+++
